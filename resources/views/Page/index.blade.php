<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css">

    <!-- JQuery JS -->
    {{-- <script src="jquery.js"></script> --}}

    <title>Travel | Landing Page</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
        <div class="container">
            <a href="#"><img src="image/globe.jpg" class="logo" alt="globe"></a>
            <a class="navbar-brand" href="#">Travel</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav" style="margin-left: auto;">
                    <a class="nav-link active" aria-current="page" href="#"><b>Home</b></a>
                    <a class="nav-link" href="#"><b>Features</b></a>
                    <a class="nav-link" href="#"><b>Gallery</b></a>
                    <a class="nav-link" href="#"><b>About Us</b></a>
                    <button type="button" class="btn btn-primary shadow button-login">Login</button>
                </div>
            </div>
        </div>
    </nav>

    <!-- Main Content -->
    <section>
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-4">
                    <h1 class="judul">Travel Yuk!!</h1>
                    <p class="mt-5">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt necessitatibus
                        exercitationem error, rem sapiente vel pariatur recusandae esse fuga saepe. Iusto voluptatum
                        perferendis aspernatur laboriosam placeat dolore dicta, accusamus quas?</p>

                    <button type="button" class="btn btn-primary btn-lg shadow button-join">More Info</button>
                </div>

                <div class="col-8">
                    <img class="poto" src="image/poto.jpg">
                </div>
            </div>
        </div>
    </section>


    <section class="features">
        <div class="container">
            <div class="row">
                <div class="col">
                    <img src="image/features1.jpg" class="image-features" alt="">
                    <h2 class="mt-4">Booking Tempat</h2>
                    <p class="mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae nobis, beatae
                        deserunt laboriosam
                        delectus illum mollitia quam fuga obcaecati. Architecto culpa doloribus, error et alias
                        dignissimos odio quisquam libero molestiae.</p>
                </div>
                <div class="col">
                    <img src="image/features2.jpg" class="image-features" alt="">
                    <h2 class="mt-4">Pesan Tiket</h2>
                    <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas consequatur explicabo
                        harum
                        accusamus magni, quo inventore nihil. Fugiat delectus quam esse dolorum nesciunt odit. Facilis
                        sunt corporis ab dolorum accusantium.</p>
                </div>
                <div class="col">
                    <img src="image/features3.jpg" class="image-features" alt="">
                    <h2 class="mt-4">Jasa Tour Guide</h2>
                    <p class="mb-5">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cupiditate asperiores
                        itaque esse iusto
                        a blanditiis totam molestias eum, provident molestiae quo ipsa beatae placeat porro assumenda et
                        facere laudantium eveniet.</p>
                </div>
            </div>
        </div>
    </section>

    {{-- Gallery Slider Image --}}
    <section>
        <div class="container mt-5 mb-5">
            <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                        class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="image/galeri1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="image/galeri2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="image/galeri3.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </section>

    <section>
        <div class="container ">
            <div class="row mt-5 mb-5">
                <div class="col-4">
                    <h1 class="judul">About Us</h1>
                    <p class="mt-4">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt necessitatibus
                        exercitationem error, rem sapiente vel pariatur recusandae esse fuga saepe.</p>

                    <button type="button" class="btn btn-primary btn-lg shadow button-about">Learn More</button>

                    <div class="contact">
                        <h3>Contact</h3>
                        <a href="#" class="contact-klik"><i class="fab fa-whatsapp wa"></i></a>
                        <a href="#" class="contact-klik"><i class="fab fa-instagram ig"></i></a>
                        <a href="#" class="contact-klik"><i class="fab fa-facebook"></i></a>
                    </div>
                </div>

                <div class="col-8">
                    <img class="image-about" src="image/about-us.jpg">
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container text-center mb-4">
            <h4 class="copyright">Copyright &copy; 2022 all right reserved Travel</h4>
        </div>
    </footer>
</body>

</html>